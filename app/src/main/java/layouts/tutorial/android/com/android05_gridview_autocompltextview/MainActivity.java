package layouts.tutorial.android.com.android05_gridview_autocompltextview;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView selection;
    private AutoCompleteTextView autoCompleteTextView;
    private String[] items = {"lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet", "lorem", "ipsum", "dolor", "sit", "amet"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        selection = (TextView) findViewById(R.id.selection);

        GridView g = (GridView) findViewById(R.id.grid);
        g.setAdapter(new MyAdapter(this, android.R.layout.simple_list_item_1, items));
        g.setOnItemClickListener(new GridOnItemClickListener());

        //AutocompleteTextView
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.edit);
        autoCompleteTextView.addTextChangedListener(new MyTextWatcher());
        autoCompleteTextView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, items));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GridOnItemClickListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selection.setText(items[position]);
            Toast.makeText(getApplicationContext(), "Selected " + items[position], Toast.LENGTH_SHORT).show();
        }
    }

    private class MyAdapter extends ArrayAdapter{
        Context ctx;

        public MyAdapter(Context context, int resource, Object[] objects) {
            super(context, resource, objects);

            this.ctx = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView label = (TextView)convertView;
            if(convertView == null){
                convertView = new TextView(ctx);
                label = (TextView)convertView;
            }

            label.setText(items[position]);

            return convertView;
        }
    }

    private class MyTextWatcher implements TextWatcher{
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            selection.setText(autoCompleteTextView.getText());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
}
